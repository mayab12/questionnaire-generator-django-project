from django.shortcuts import render
#from personal.models import Question

from account.models import Account

# Create your views here.

def home_screen_view(request):

    context = {}

    #context['some_string'] = "this is some string that im passing to the view"

    #context = {
    #    'some_string': 'sdfdf'
    #}

    # list_of_values = []
    # list_of_values.append("first")
    # list_of_values.append("dwa")
    # list_of_values.append("try")
    # list_of_values.append("cztrete")
    # context['list_of_values'] = list_of_values

    # questions = Question.object.all()
    # questions = Question.objects.all()
    # context['questions'] = questions

    accounts = Account.objects.all()
    context['accounts'] = accounts

    return render(request, "personal/home.html", context)